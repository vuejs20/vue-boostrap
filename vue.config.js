module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160281/learn_bootstrap/'
    : '/'
}